
import ru.nsu.ermakova.stackcalculator.StackCalculator;

import java.io.IOException;
import java.util.logging.LogManager;

public class Calculator {
    public static void main(String[] args) {
        try {
            LogManager.getLogManager().readConfiguration(
                    Calculator.class.getResourceAsStream("ru/nsu/ermakova/stackcalculator/logging.properties"));
        } catch (IOException e) {
            System.err.println("Could not setup logger configuration: " + e.toString());
        }
        StackCalculator calc = new StackCalculator();
        if (args.length > 0) calc.setFileInput(args[0]);
        else calc.setConsoleInput();
        calc.process();
    }
}
