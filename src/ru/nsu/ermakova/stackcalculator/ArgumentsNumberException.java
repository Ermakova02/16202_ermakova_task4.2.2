package ru.nsu.ermakova.stackcalculator;

public class ArgumentsNumberException extends Exception {
    public ArgumentsNumberException(String message) {
        super(message);
    }
}
