package ru.nsu.ermakova.stackcalculator;

import java.util.ArrayList;
import java.util.logging.Logger;

public abstract class Command {
    protected static Logger log = Logger.getLogger(Command.class.getName());
    protected ArrayList<String> args;
    protected CalculatorContext cntx;

    public abstract void execute() throws ArgumentsNumberException, EmptyStackException, MathException, DefineException;

    public void setArguments(ArrayList<String> args) { this.args = args; }
    public void setContext(CalculatorContext cntx) { this.cntx = cntx; }
}
