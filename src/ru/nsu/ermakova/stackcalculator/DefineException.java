package ru.nsu.ermakova.stackcalculator;

public class DefineException extends Exception {
    public DefineException(String message) {
        super(message);
    }
}
