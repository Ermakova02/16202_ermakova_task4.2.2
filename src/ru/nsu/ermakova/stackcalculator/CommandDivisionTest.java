package ru.nsu.ermakova.stackcalculator;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class CommandDivisionTest {
    public static final String CMD_DIVISION = "/";
    public static final String CMD_PUSH = "PUSH";
    public static final String CMD_DEFINE = "DEFINE";
    @Test
    public void testexecute() {
        Command cmd = null;
        ArrayList<String> args = new ArrayList<>();
        CalculatorContext cntx = new CalculatorContext();
        CommandFactory factory = new CommandFactory();
        try {
            // Round 1
            cmd = factory.getCommand(CMD_PUSH);
            args.add("2");
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute();

            cmd = factory.getCommand(CMD_PUSH);
            args.clear();
            args.add("197.92");
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute();

            cmd = factory.getCommand(CMD_DIVISION);
            args.clear();
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute();

            Double val = 98.96;
            assertFalse(cntx.stack.empty()); // Stack is not empty
            assertEquals(val, cntx.stack.peek());

            // Round 2
            cmd = factory.getCommand(CMD_PUSH);
            args.clear();
            cntx.stack.clear();
            cntx.list.clear();
            args.add("1");
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute();

            cmd = factory.getCommand(CMD_PUSH);
            args.clear();
            args.add("-2.89");
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute();

            cmd = factory.getCommand(CMD_DIVISION);
            args.clear();
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute();

            val = -2.89;
            assertFalse(cntx.stack.empty()); // Stack is not empty
            assertEquals(val, cntx.stack.peek());

            // Round 3
            cmd = factory.getCommand(CMD_DEFINE);
            args.clear();
            cntx.stack.clear();
            cntx.list.clear();
            args.add("Var_1");
            args.add("-4.0");
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute();

            cmd = factory.getCommand(CMD_PUSH);
            args.clear();
            args.add("-2.0");
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute();

            cmd = factory.getCommand(CMD_PUSH);
            args.clear();
            args.add("Var_1");
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute();

            cmd = factory.getCommand(CMD_DIVISION);
            args.clear();
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute();

            val = 2.0;
            assertFalse(cntx.stack.empty()); // Stack is not empty
            assertEquals(val, cntx.stack.peek());
        } catch (ClassNotFoundException e) {
            System.out.println("Ошибка: " + e.getMessage());
        } catch (CommandNotFoundException e) {
            System.out.println("Ошибка: " + e.getMessage());
        } catch (ArgumentsNumberException e) {
            System.out.println("Ошибка: " + e.getMessage());
        } catch (EmptyStackException e) {
            System.out.println("Ошибка: " + e.getMessage());
        } catch (MathException e) {
            System.out.println("Ошибка: " + e.getMessage());
        } catch (DefineException e) {
            System.out.println("Ошибка: " + e.getMessage());
        } catch (ArithmeticException e) {
            System.out.println("Ошибка: " + e.getMessage());
        } catch (NumberFormatException e) {
            System.out.println("Ошибка: " + e.getMessage());
        }
    }

}

