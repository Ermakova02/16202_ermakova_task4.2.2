package ru.nsu.ermakova.stackcalculator;

import java.util.LinkedList;

public class Stack<T> {
    private LinkedList<T> storage = new LinkedList<T>();
    public void push(T v) { storage.addFirst(v); }
    public T peek() { return storage.getFirst(); }
    public T pop() {return storage.removeFirst(); }
    public boolean empty() { return storage.isEmpty(); }
    public void clear() { storage.clear(); }
    public String toString() { return storage.toString(); }
}
