package ru.nsu.ermakova.stackcalculator;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StackCalculator {
    private static Logger log = Logger.getLogger(StackCalculator.class.getName());
    public static final String EXIT_LINE = "EXIT";
    public static final char COMMENT_SIGN = '#';
    public static final char SPACE_SIGN = ' ';
    private boolean consoleInput;
    private String inputFileName;
    public StackCalculator() {
        consoleInput = true;
        inputFileName = "";
    }
    public void setConsoleInput()
    {
        log.log(Level.INFO, "setConsoleInput");
        consoleInput = true;
        inputFileName = "";
    }
    public void setFileInput(String name) {
        log.log(Level.INFO, "setFileInput(" + name + ")");
        consoleInput = false;
        inputFileName = name;
    }
    public void process(){
        Scanner in = null;
        if (consoleInput) {
            in = new Scanner(System.in);
        }
        else {
            try {
                in = new Scanner(new FileReader(inputFileName));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        String line = "";
        String substr = "";
        String cmdName = "";
        Command cmd = null;
        ArrayList<String> args = new ArrayList<>();
        CalculatorContext cntx = new CalculatorContext();
        CommandFactory factory = new CommandFactory();
        while (in.hasNext()) {
            line = in.nextLine();
            log.log(Level.INFO, "Read line: " + line);
            if (line.equals(EXIT_LINE)) break;
            if (line.length() == 0) continue;
            if (line.charAt(0) == COMMENT_SIGN) continue;
            int beginIndex = line.indexOf(SPACE_SIGN);
            int endIndex = 0;
            args.clear();
            if (beginIndex == -1) {
                cmdName = line;
            }
            else {
                cmdName = line.substring(0, beginIndex);
                log.log(Level.INFO, "Command Name: " + cmdName);
                do {
                    beginIndex++;
                    endIndex = line.indexOf(SPACE_SIGN, beginIndex);
                    if (endIndex == -1) substr = line.substring(beginIndex);
                    else substr = line.substring(beginIndex, endIndex);
                    if (substr.length() > 0) {
                        log.log(Level.INFO, "Argument: " + substr);
                        args.add(substr);
                    }
                    beginIndex = endIndex;
                } while (beginIndex != -1);
            }
            try {
                cmd = factory.getCommand(cmdName);
                cmd.setArguments(args);
                cmd.setContext(cntx);
                cmd.execute();
            } catch (ClassNotFoundException e) {
                System.out.println("Ошибка: " + e.getMessage());
                log.log(Level.SEVERE, "Class not found.", e);
            } catch (CommandNotFoundException e) {
                System.out.println("Ошибка: " + e.getMessage());
                log.log(Level.SEVERE, "Command " + cmdName + " not found.", e);
            } catch (ArgumentsNumberException e) {
                System.out.println("Ошибка: " + e.getMessage());
                log.log(Level.SEVERE, "Wrong arguments number", e);
            } catch (EmptyStackException e) {
                System.out.println("Ошибка: " + e.getMessage());
                log.log(Level.SEVERE, "Stack is empty. Operation " + cmdName + " is not allowed.", e);
            } catch (MathException e) {
                System.out.println("Ошибка: " + e.getMessage());
                log.log(Level.SEVERE, "Math error.", e);
            } catch (DefineException e) {
                System.out.println("Ошибка: " + e.getMessage());
                log.log(Level.SEVERE, "Command DEFINE error.", e);
            } catch (ArithmeticException e) {
                System.out.println("Ошибка: " + e.getMessage());
                log.log(Level.SEVERE, "Arithmetic error.", e);
            } catch (NumberFormatException e) {
                System.out.println("Ошибка: " + e.getMessage());
                log.log(Level.SEVERE, "Format number error.", e);
            }
        }
    }
}
