package ru.nsu.ermakova.stackcalculator;

import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class CommandPrintTest {
    public static final String CMD_PUSH = "PUSH";
    public static final String CMD_PRINT = "PRINT";
    @Test
    public void testexecute() {
        Command cmd = null;
        ArrayList<String> args = new ArrayList<>();
        CalculatorContext cntx = new CalculatorContext();
        CommandFactory factory = new CommandFactory();
        try {
            // Round 1
            cmd = factory.getCommand(CMD_PUSH);
            args.add("5");
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute();

            PrintStream st = new PrintStream(new FileOutputStream("unit_test_temp_file"));
            System.setOut(st);

            cmd = factory.getCommand(CMD_PRINT);
            args.clear();
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute();

            Scanner in = null;
            in = new Scanner(new FileReader("unit_test_temp_file"));
            int numLines = 0;
            String line = "";
            while (in.hasNext()) {
                line = in.nextLine();
                numLines++;
            }
            assertEquals(1, numLines); // Only 1 line in output
            assertFalse(cntx.stack.empty()); // Stack is not empty
            assertEquals("5.0", line); // "5.0" printed
        } catch (ClassNotFoundException e) {
            System.out.println("Ошибка: " + e.getMessage());
        } catch (CommandNotFoundException e) {
            System.out.println("Ошибка: " + e.getMessage());
        } catch (ArgumentsNumberException e) {
            System.out.println("Ошибка: " + e.getMessage());
        } catch (EmptyStackException e) {
            System.out.println("Ошибка: " + e.getMessage());
        } catch (MathException e) {
            System.out.println("Ошибка: " + e.getMessage());
        } catch (DefineException e) {
            System.out.println("Ошибка: " + e.getMessage());
        } catch (ArithmeticException e) {
            System.out.println("Ошибка: " + e.getMessage());
        } catch (NumberFormatException e) {
            System.out.println("Ошибка: " + e.getMessage());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}