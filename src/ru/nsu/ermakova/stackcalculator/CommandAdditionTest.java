package ru.nsu.ermakova.stackcalculator;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.ArrayList;

public class CommandAdditionTest {
    public static final String CMD_ADDITION = "+";
    public static final String CMD_PUSH = "PUSH";
    public static final String CMD_DEFINE = "DEFINE";
    @Test
    public void testexecute() {
        Command cmd = null;
        ArrayList<String> args = new ArrayList<>();
        CalculatorContext cntx = new CalculatorContext();
        CommandFactory factory = new CommandFactory();
        try {
            // Round 1
            cmd = factory.getCommand(CMD_PUSH);
            args.add("5");
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute();

            cmd = factory.getCommand(CMD_PUSH);
            args.clear();
            args.add("7");
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute();

            cmd = factory.getCommand(CMD_ADDITION);
            args.clear();
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute();

            Double val = 12.0;
            assertFalse(cntx.stack.empty()); // Stack is not empty
            assertEquals(val, cntx.stack.peek()); // 5 + 7 = 12

            // Round 2
            cmd = factory.getCommand(CMD_PUSH);
            args.clear();
            cntx.stack.clear();
            cntx.list.clear();
            args.add("8735.81");
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute();

            cmd = factory.getCommand(CMD_PUSH);
            args.clear();
            args.add("-79919.2367");
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute();

            cmd = factory.getCommand(CMD_ADDITION);
            args.clear();
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute();

            val = -71183.4267;
            assertFalse(cntx.stack.empty()); // Stack is not empty
            assertEquals(val, cntx.stack.peek()); // 8735.81 + (-79919.2367) = -71183.4267

            // Round 3
            cmd = factory.getCommand(CMD_DEFINE);
            args.clear();
            cntx.stack.clear();
            cntx.list.clear();
            args.add("Var_1");
            args.add("-16.245");
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute();

            cmd = factory.getCommand(CMD_PUSH);
            args.clear();
            args.add("23.129");
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute();

            cmd = factory.getCommand(CMD_PUSH);
            args.clear();
            args.add("Var_1");
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute();

            cmd = factory.getCommand(CMD_ADDITION);
            args.clear();
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute();

            val = 6.884;
            assertFalse(cntx.stack.empty()); // Stack is not empty
            assertEquals(val, cntx.stack.peek()); // 23.129 + (-16.245) = 6.884
        } catch (ClassNotFoundException e) {
            System.out.println("Ошибка: " + e.getMessage());
        } catch (CommandNotFoundException e) {
            System.out.println("Ошибка: " + e.getMessage());
        } catch (ArgumentsNumberException e) {
            System.out.println("Ошибка: " + e.getMessage());
        } catch (EmptyStackException e) {
            System.out.println("Ошибка: " + e.getMessage());
        } catch (MathException e) {
            System.out.println("Ошибка: " + e.getMessage());
        } catch (DefineException e) {
            System.out.println("Ошибка: " + e.getMessage());
        } catch (ArithmeticException e) {
            System.out.println("Ошибка: " + e.getMessage());
        } catch (NumberFormatException e) {
            System.out.println("Ошибка: " + e.getMessage());
        }
    }

}
