package ru.nsu.ermakova.stackcalculator;

import java.util.logging.Level;

public class CommandDefine extends Command {
    public void execute() throws ArgumentsNumberException, EmptyStackException, MathException, DefineException {
        log.log(Level.INFO, "Arguments number: " + args.size());
        if (args.size() != 2) throw new ArgumentsNumberException("Количество аргументов не равно двум");
        Double num = Double.valueOf(args.get(1));
        log.log(Level.INFO, "Put to list of variables " + args.get(0) + " = " + num);
        cntx.list.put(args.get(0), num);
    }
}
