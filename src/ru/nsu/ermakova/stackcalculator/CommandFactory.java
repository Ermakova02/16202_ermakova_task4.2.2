package ru.nsu.ermakova.stackcalculator;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CommandFactory {
    private static Logger log = Logger.getLogger(CommandFactory.class.getName());
    public Command getCommand(String cmdName) throws ClassNotFoundException, CommandNotFoundException {
        log.log(Level.INFO, "getCommand(" + cmdName + ")");
        Properties prop = new Properties();
        String className = "";
        Command cmd = null;
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("ru/nsu/ermakova/stackcalculator/config.properties");
            prop.load(inputStream);
            className = prop.getProperty(cmdName);
            if (className == null)
                throw new CommandNotFoundException("Не найдена команда " + cmdName);
            cmd = (Command) Class.forName(className).newInstance();
        } catch (IOException e) {
            log.log(Level.SEVERE, "I/O Exception", e);
            e.printStackTrace();
        } catch (InstantiationException e) {
            log.log(Level.SEVERE, "Instantiation Exception", e);
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            log.log(Level.SEVERE, "Illegal Access Exception", e);
            e.printStackTrace();
        }
/*        Command cmd = null;
        switch(cmdName) {
            case "POP": cmd = new CommandPop(); break;
            case "PUSH": cmd = new CommandPush(); break;
            case "+": cmd = new CommandAddition(); break;
            case "-": cmd = new CommandSubtraction(); break;
            case "*": cmd = new CommandMultiplication(); break;
            case "/": cmd = new CommandDivision(); break;
            case "SQRT": cmd = new CommandSqroot(); break;
            case "PRINT": cmd = new CommandPrint(); break;
            case "DEFINE": cmd = new CommandDefine(); break;
        }*/
        return cmd;
    }
}
