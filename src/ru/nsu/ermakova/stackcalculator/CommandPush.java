package ru.nsu.ermakova.stackcalculator;

import java.util.logging.Level;

public class CommandPush extends Command {
    public void execute() throws ArgumentsNumberException, EmptyStackException, MathException, DefineException {
        log.log(Level.INFO, "Arguments number: " + args.size());
        if (args.size() != 1) throw new ArgumentsNumberException("Количество аргументов не равно одному");
        Double num = 0.0;
        try {
            num = Double.valueOf(args.get(0));
        } catch (NumberFormatException e) {
            if (cntx.list.containsKey(args.get(0))) {
                num = cntx.list.get(args.get(0));
            }
            else throw new DefineException("Переменная " + args.get(0) + " не найдена среди определённых переменных");
        }
        cntx.stack.push(num);
        log.log(Level.INFO, "Push to stack number: " + num);
    }
}
