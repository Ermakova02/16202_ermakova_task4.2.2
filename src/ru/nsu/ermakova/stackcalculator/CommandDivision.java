package ru.nsu.ermakova.stackcalculator;

import java.util.logging.Level;

public class CommandDivision extends Command {
    public void execute() throws ArgumentsNumberException, EmptyStackException, MathException, DefineException {
        log.log(Level.INFO, "Arguments number: " + args.size());
        if (args.size() != 0) throw new ArgumentsNumberException("Количество аргументов не равно нулю");
        if (cntx.stack.empty()) log.log(Level.INFO, "Stack is empty.");
        else log.log(Level.INFO, "Stack is not empty.");
        if (cntx.stack.empty()) throw new EmptyStackException("Стек пустой");
        Double num1 = cntx.stack.pop();
        log.log(Level.INFO, "Pop from stack number: " + num1);
        if (cntx.stack.empty()) throw new EmptyStackException("Стек пустой");
        Double num2 = cntx.stack.pop();
        log.log(Level.INFO, "Pop from stack number: " + num2);
        if (num2 == 0) throw new MathException("Деление на ноль");
        num1 /= num2;
        log.log(Level.INFO, "Push to stack number: " + num1);
        cntx.stack.push(num1);
    }
}
