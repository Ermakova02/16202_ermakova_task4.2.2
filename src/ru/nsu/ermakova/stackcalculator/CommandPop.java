package ru.nsu.ermakova.stackcalculator;

import java.util.logging.Level;

public class CommandPop extends Command {
    public void execute() throws ArgumentsNumberException, EmptyStackException, MathException, DefineException {
        log.log(Level.INFO, "Arguments number: " + args.size());
        if (args.size() != 0) throw new ArgumentsNumberException("Количество аргументов не равно нулю");
        if (cntx.stack.empty()) log.log(Level.INFO, "Stack is empty.");
        else log.log(Level.INFO, "Stack is not empty.");
        if (cntx.stack.empty()) throw new EmptyStackException("Стек пустой");
        log.log(Level.INFO, "Pop from stack number");
        cntx.stack.pop();
    }
}
