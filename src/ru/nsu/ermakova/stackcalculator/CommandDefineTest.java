package ru.nsu.ermakova.stackcalculator;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.ArrayList;

public class CommandDefineTest {
    public static final String CMD_ADDITION = "+";
    public static final String CMD_SUBTRACTION = "-";
    public static final String CMD_PUSH = "PUSH";
    public static final String CMD_DEFINE = "DEFINE";
    @Test
    public void testexecute() {
        Command cmd = null;
        ArrayList<String> args = new ArrayList<>();
        CalculatorContext cntx = new CalculatorContext();
        CommandFactory factory = new CommandFactory();
        try {
            // Round 1
            cmd = factory.getCommand(CMD_DEFINE);
            args.add("Var-1");
            args.add("28.57");
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute();

            cmd = factory.getCommand(CMD_DEFINE);
            args.clear();
            args.add("x");
            args.add("71.22");
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute();

            cmd = factory.getCommand(CMD_DEFINE);
            args.clear();
            args.add("X");
            args.add("0.0245");
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute();

            cmd = factory.getCommand(CMD_PUSH);
            args.clear();
            args.add("x"); // 71.22
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute();

            cmd = factory.getCommand(CMD_PUSH);
            args.clear();
            args.add("X"); // 0.0245
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute();

            cmd = factory.getCommand(CMD_PUSH);
            args.clear();
            args.add("Var-1"); // 28.57
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute();

            cmd = factory.getCommand(CMD_ADDITION);
            args.clear();
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute(); // 28.57 + 0.0245 = 28.5945

            cmd = factory.getCommand(CMD_SUBTRACTION);
            args.clear();
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute(); // 28.5945 - 71.22 = -42.6255

            Double val = -42.6255;
            assertFalse(cntx.stack.empty()); // Stack is not empty
            assertEquals(val, cntx.stack.peek()); // -42.6255

            // Round 2
            cmd = factory.getCommand(CMD_DEFINE);
            args.clear();
            args.add("Var-1");
            args.add("101");
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute();

            cmd = factory.getCommand(CMD_PUSH);
            args.clear();
            args.add("Var-1"); // 101.0
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute();

            cmd = factory.getCommand(CMD_ADDITION);
            args.clear();
            cmd.setArguments(args);
            cmd.setContext(cntx);
            cmd.execute(); // 101.0 + (-42.6255) = 58,3745

            val = 58.3745;
            assertFalse(cntx.stack.empty()); // Stack is not empty
            assertEquals(val, cntx.stack.peek()); // 58.3745
        } catch (ClassNotFoundException e) {
            System.out.println("Ошибка: " + e.getMessage());
        } catch (CommandNotFoundException e) {
            System.out.println("Ошибка: " + e.getMessage());
        } catch (ArgumentsNumberException e) {
            System.out.println("Ошибка: " + e.getMessage());
        } catch (EmptyStackException e) {
            System.out.println("Ошибка: " + e.getMessage());
        } catch (MathException e) {
            System.out.println("Ошибка: " + e.getMessage());
        } catch (DefineException e) {
            System.out.println("Ошибка: " + e.getMessage());
        } catch (ArithmeticException e) {
            System.out.println("Ошибка: " + e.getMessage());
        } catch (NumberFormatException e) {
            System.out.println("Ошибка: " + e.getMessage());
        }
    }

}
